package app.user;

import app.authentication.Session;
import app.controllers.PictureController;
import app.event.create.EventController;
import com.google.gson.Gson;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.protocol.HTTP;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import app.authentication.AuthService;
import app.authentication.SessionService;

import javax.inject.Inject;
import javax.persistence.PersistenceException;
import javax.transaction.*;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by lacka on 06/05/15.
 */
@Path("/user")
public class UserController {
    @Inject
    private UserService userService;
    @Inject
    private AuthService authService;
    @Inject
    private SessionService sessionService;

    String uploadDir = ".avatars/";



    @POST
    @Path("/get")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(UserRequest req) {
        String sessionId = req.getSessionId();
        User user = null;
        if (sessionId != null) {
            Session session = sessionService.get(UUID.fromString(sessionId));
            user = userService.get(session.getValue());
        }

        if (user != null) {
            return Response.status(200).entity(new UserResponse(user)).build();
        } else {
            return Response.status(500).entity("User not found").build();
        }
    }

    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(User newUser) {
        Response ret = null;
        try {
            User user = userService.get(newUser.getId());
            user.update(newUser);
            userService.update(user);
            return Response.status(200).entity("User updated").build();
        } catch (PersistenceException e) {
            e.printStackTrace();
            ret = Response.status(403).entity("Update failed").build();
        } catch (Exception e) {
            e.printStackTrace();
            ret = Response.status(500).build();
        }
        return ret;
    }

    private InputPart getInputPart(Map<String, List<InputPart>> params) {
        InputPart imagePart = null;
        if (params.containsKey("image")) {
            imagePart = params.get("image").get(0);
        }
        return imagePart;
    }

    private String processImage(String userName, InputPart part) throws IOException, HeuristicRollbackException, HeuristicMixedException, NotSupportedException, RollbackException, SystemException {

        generateDirStructure(PictureController.UPLOAD_BASE + uploadDir);
        String fileName = "dummy.png";
        if (part != null) {
            fileName = generateFileName(userName, part);
            saveToDisk(part, PictureController.UPLOAD_BASE + uploadDir, fileName);
        }
        return fileName;
    }

    private void generateDirStructure(String uploadDir) {
        new File(uploadDir).mkdirs();
    }

    private String generateFileName(String userName, InputPart part) {
        return new String(Base64.encodeBase64(userName.getBytes()))
                + "." + part.getMediaType().getSubtype();
    }

    private void saveToDisk(InputPart part, String uploadDir, String fileName) throws IOException {
        InputStream inputStream = part.getBody(InputStream.class, null);
        FileOutputStream outputStream = new FileOutputStream(uploadDir + fileName);
        byte[] b = new byte[1024];
        int len;
        while ((len = inputStream.read(b)) != -1) {
            outputStream.write(b, 0, len);
        }
        outputStream.flush();
        outputStream.close();
        inputStream.close();
    }
}
