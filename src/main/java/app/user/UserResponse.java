package app.user;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lacka on 20/01/16.
 */
public class UserResponse implements Serializable {
    private long id;
    private String userName;
    private String userEmail;
    private String nickName;
    private String avatarUrl = "";
    private String subject = "";
    private String message = "";
    private List<String> emails;
    private List<String> smsNumbers;
    private String voiceCallNumber = "";
    private String reward = "";
    private Long createDate;

    public UserResponse(User user) {
        this.setId(user.getId());
        this.setUserName(user.getUserName());
        this.setUserEmail(user.getUserEmail());
        this.setNickName(user.getNickName());
        this.setAvatarUrl(user.getAvatar());
        this.setSubject(user.getSubject());
        this.setMessage(user.getMessage());
        this.setEmails(user.getEmails());
        this.setSmsNumbers(user.getSmsNumbers());
        this.setVoiceCallNumber(user.getVoiceCallNumber());
        this.setReward(user.getReward());
        this.setCreateDate(user.getCreateDate());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public List<String> getSmsNumbers() {
        return smsNumbers;
    }

    public void setSmsNumbers(List<String> smsNumbers) {
        this.smsNumbers = smsNumbers;
    }

    public String getVoiceCallNumber() {
        return voiceCallNumber;
    }

    public void setVoiceCallNumber(String voiceCallNumber) {
        this.voiceCallNumber = voiceCallNumber;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }
}
