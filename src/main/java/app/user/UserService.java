package app.user;

import javax.annotation.ManagedBean;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.*;

/**
 * Created by lacka on 06/05/15.
 */
@ManagedBean
public class UserService {
    @PersistenceContext
    private EntityManager em;

    @Resource
    private UserTransaction utx;

    public User register(User u) throws SystemException, NotSupportedException, HeuristicRollbackException, HeuristicMixedException, RollbackException {
        utx.begin();
        em.persist(u);
        em.flush();
        utx.commit();
        return u;
    }

    public void update(User u) throws SystemException, NotSupportedException, HeuristicRollbackException, HeuristicMixedException, RollbackException {
        User user = em.find(User.class, u.getId());
        user.update(u);
        utx.begin();
        em.merge(user);
        utx.commit();
    }

    public User get(Long id) {
        return em.find(User.class, id);
    }
}
