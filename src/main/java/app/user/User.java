package app.user;

import app.models.requests.RegistrationRQ;
import app.utils.SecurePair;
import com.google.gson.Gson;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users", uniqueConstraints = @UniqueConstraint(columnNames = {"userName", "userEmail"}))
public class User implements Serializable {
	
	private static final long serialVersionUID = -3479998433047310430L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(unique = true)
	private String userName;

	@Column(unique = true)
	private String userEmail;
	
	private String nickName = "";
	
	private String password;


	private String avatarUrl = "";
	
	private String salt;
	
	private String subject = "";
	
	private String message = "";

	@Fetch(FetchMode.SUBSELECT)
	@ElementCollection(fetch=FetchType.EAGER)
	private List<String> emails = new ArrayList<String>();

	@Fetch(FetchMode.SUBSELECT)
	@ElementCollection(fetch=FetchType.EAGER)
	private List<String> smsNumbers = new ArrayList<String>();
	
	private String voiceCallNumber = "";
	
	private String reward = "";

	private Long createDate;

	public User() {}

	public User(RegistrationRQ req, SecurePair pair) {
		this.setUserName(req.getUsername());
		this.setUserEmail(req.getEmail());
		this.setNickName(req.getNickname());
		this.setPassword(pair.getPassword());
		this.setSalt(pair.getSalt());
		this.setCreateDate(System.currentTimeMillis());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getEmails() {
		return emails;
	}

	public void setEmails(List<String> emails) {
		this.emails = emails;
	}

	public List<String> getSmsNumbers() {
		return smsNumbers;
	}

	public void setSmsNumbers(List<String> smsNumbers) {
		this.smsNumbers = smsNumbers;
	}

	public String getVoiceCallNumber() {
		return voiceCallNumber;
	}

	public void setVoiceCallNumber(String voiceCallNumber) {
		this.voiceCallNumber = voiceCallNumber;
	}

	public String getReward() {
		return reward;
	}

	public void setReward(String reward) {
		this.reward = reward;
	}

	public Long getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Long createDate) {
		this.createDate = createDate;
	}

	public String getAvatar() {
		return avatarUrl;
	}

	public void setAvatar(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public void update(User user) {
		this.avatarUrl = user.avatarUrl;
		this.userName = user.userName;
		this.userEmail = user.userEmail;
		this.nickName = user.nickName;
		this.subject = user.subject;
		this.message = user.message;
		this.emails = user.emails;
		this.smsNumbers = user.smsNumbers;
		this.voiceCallNumber = user.voiceCallNumber;
		this.reward = user.reward;
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}
