package app.models;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "notifiedfromcreateevent")
public class NotifiedFromCreateEvent implements Serializable {

	private static final long serialVersionUID = -962600514103715356L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String eventID;

	private String origin;

	private String destination;

	public NotifiedFromCreateEvent() {
	}

	public NotifiedFromCreateEvent(String eventID, String origin, String destination){
		this.eventID = eventID;
		this.origin = origin;
		this.destination = destination;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getEventID() {
		return eventID;
	}

	public void setEventID(String eventID) {
		this.eventID = eventID;
	}
}
