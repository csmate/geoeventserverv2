package app.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "notifs")
public class Notification implements Serializable {

	private static final long serialVersionUID = -2532010069529628425L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private Long notifTime;
	
	private Long deviceID;
	
	private Long eventID;

	public Notification() {}
	
	public Notification(Long notifTime, Long deviceID, Long eventID) {
		super();
		this.notifTime = notifTime;
		this.deviceID = deviceID;
		this.eventID = eventID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNotifTime() {
		return notifTime;
	}

	public void setNotifTime(Long notifTime) {
		this.notifTime = notifTime;
	}

	public Long getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(Long deviceID) {
		this.deviceID = deviceID;
	}

	public Long getEventID() {
		return eventID;
	}

	public void setEventID(Long eventID) {
		this.eventID = eventID;
	}

	
	
}
