package app.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "pictures")
public class Picture implements Serializable {

	private static final long serialVersionUID = 4806460675549388627L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String fileName;

	private String fileData;
	
	private String deviceID;
	
	private String eventID;
	
	private Long creationTime;

	public Picture() {}
	
	public Picture(String fileName, String fileData, String deviceID, String eventID) {
		this.fileName = fileName; //url postfix
		this.fileData = fileData;
		this.deviceID = deviceID;
		this.eventID = eventID;
		this.creationTime = System.currentTimeMillis();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileData() {
		return fileData;
	}

	public void setFileData(String fileData) {
		this.fileData = fileData;
	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public String getEventID() {
		return eventID;
	}

	public void setEventID(String eventID) {
		this.eventID = eventID;
	}

	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}
	
}
