package app.services;

import app.models.Config;

import javax.ejb.Stateless;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;

/**
 * Created by lacka on 06/05/15.
 */
@Stateless
public class ConfigService {
    @PersistenceContext
    private EntityManager em;

    public List<Config> getAll() {
        TypedQuery<Config> query = em.createQuery("SELECT u FROM Config u", Config.class);
        return query.getResultList();
    }

    public Config get(String key) {
        TypedQuery<Config> query = em.createQuery("SELECT u FROM Config u WHERE u.key = :key", Config.class).setParameter("key", key);
        return query.getSingleResult();
    }

    public void add(Config cfg) {
        long size = getAll().size();

        em.persist(cfg);
    }
}
