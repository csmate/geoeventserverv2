package app.services;

import app.models.Picture;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 * Created by lacka on 06/05/15.
 */
@Stateless
public class PictureService {
    @PersistenceContext
    private EntityManager em;

    public Picture get(String eventId) {
        Picture ret = null;
        try {
            ret = em.createQuery("select p from Picture p where p.eventID = :eventId order by p.creationTime desc", Picture.class)
                    .setParameter("eventId", eventId).setMaxResults(1).getResultList().get(0);
        } catch (Exception e) {}
        return ret;
    }

    public Picture getAttachedPicture(Long id) {
        return em.find(Picture.class, id);
    }

    public Picture getByFileName(String fileName) {
        Picture ret = null;
        try {
            ret = em.createQuery("select p from Picture p where p.fileName = :name", Picture.class)
                    .setParameter("name", fileName).getSingleResult();
        } catch (NoResultException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public boolean save(Picture pic) {
        boolean ret = true;
        try {
            em.persist(pic);
        } catch (Exception e){
            ret = false;
        }
        return ret;
    }

    public boolean merge(Picture pic) {
        boolean ret = true;
        try {
            em.merge(pic);
        } catch (Exception e){
            ret = false;
        }
        return ret;
    }
}
