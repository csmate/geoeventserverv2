package app.authentication;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

/**
 * Created by lacka on 27/08/15.
 */
@Entity
public class Session implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Long id;

    @Column(name = "key_")
    private UUID key;

    private Long value;

    private Long timestamp;

    public Session() {}

    public Session(UUID key, Long value) {
        this.key = key;
        this.value = value;
        this.timestamp = System.currentTimeMillis();
    }

    public Long getId() {
        return id;
    }

    public UUID getKey() {
        return key;
    }

    public void setKey(UUID key) {
        this.key = key;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
