package app.authentication;

/**
 * Created by lacka on 23/01/16.
 */
public class AuthenticationError {
    private String username;
    private String error;

    public AuthenticationError() {}

    public AuthenticationError(String username) {
        this.username = username;
        this.error = "User not found";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
