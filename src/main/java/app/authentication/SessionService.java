package app.authentication;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.UUID;

@Stateless
public class SessionService {
    @PersistenceContext
    private EntityManager em;

    public Session get(UUID key) {
        TypedQuery<Session> query = em.createQuery("SELECT s FROM Session s WHERE s.key = :key", Session.class).setParameter("key", key);
        return query.getSingleResult();
    }

    public void set(UUID key, Long value) {
        Session s = new Session(key, value);
        em.persist(s);
    }

    public void remove(UUID key) {
        Session s = get(key);
        Session rem = em.find(Session.class, s.getId());
        em.remove(rem);
    }
}
