package app.authentication;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * Created by lacka on 06/05/15.
 */
@Stateless
public class AuthService {
    @PersistenceContext
    private EntityManager em;

    public Long getUser(String username, String pw) {
        Long ret = null;
        try {
            TypedQuery<Long> query =
                em.createQuery("select u.id from User u where u.userName = :u and u.password = :p", Long.class)
                    .setParameter("u", username)
                    .setParameter("p", pw);
            ret = query.getSingleResult();
        } catch (NoResultException e) {}

        return ret;
    }

    public String getSalt(String username) {
        String salt = null;
        try{
            salt = em.createQuery("select u.salt from User u where u.userName = :u", String.class)
                .setParameter("u", username)
                .getSingleResult();
        } catch (NoResultException e) {}

        return salt;
    }
}
