package app.device;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "devices", uniqueConstraints = @UniqueConstraint(columnNames = {"deviceID", "profileID"}))
public class Device implements Serializable {

	private static final long serialVersionUID = -2127772688882444782L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String deviceID;

	private Long profileID;

	private Long loginTime;

	@Column(columnDefinition = "boolean default true")
	private Boolean recvPush;
	
	@Column(columnDefinition = "boolean default true")
	private Boolean gpsEnabled;

	private Long gpsLastRefreshTime;

	private Double gpsLong;

	private Double gpsLat;
	
	@Column(columnDefinition = "boolean default true")
	private Boolean isActive;

	public Device() {}

	public Device(String deviceID, Long profileID, Long loginTime,
			Boolean recvPush, Long gPSLastRefreshTime, Double gPSLong,
			Double gPSLat) {
		this.deviceID = deviceID;
		this.profileID = profileID;
		this.loginTime = loginTime;
		this.recvPush = recvPush;
		this.gpsLastRefreshTime = gPSLastRefreshTime;
		this.gpsLong = gPSLong;
		this.gpsLat = gPSLat;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public Long getProfileID() {
		return profileID;
	}

	public void setProfileID(Long profileID) {
		this.profileID = profileID;
	}

	public Long getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Long loginTime) {
		this.loginTime = loginTime;
	}

	public Boolean getRecvPush() {
		return recvPush;
	}

	public void setRecvPush(Boolean recvPush) {
		this.recvPush = recvPush;
	}
	
	public Boolean getGpsEnabled() {
		return gpsEnabled;
	}

	public void setGpsEnabled(Boolean gpsEnabled) {
		this.gpsEnabled = gpsEnabled;
	}

	public Long getGpsLastRefreshTime() {
		return gpsLastRefreshTime;
	}

	public void setGpsLastRefreshTime(Long gpsLastRefreshTime) {
		this.gpsLastRefreshTime = gpsLastRefreshTime;
	}

	public Double getGpsLong() {
		return gpsLong;
	}

	public void setGpsLong(Double gpsLong) {
		this.gpsLong = gpsLong;
	}

	public Double getGpsLat() {
		return gpsLat;
	}

	public void setGpsLat(Double gpsLat) {
		this.gpsLat = gpsLat;
	}

	public Boolean isActive() {
		return isActive;
	}

	public void setActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public void update(Device device) {
		if (device.getRecvPush() != null) this.setRecvPush(device.getRecvPush());
		if (device.getGpsEnabled() != null) this.setGpsEnabled(device.getGpsEnabled());
		if (device.getDeviceID() != null) this.setDeviceID(device.getDeviceID());
		if (device.getProfileID() != null) this.setProfileID(device.getProfileID());
		if (device.getGpsLastRefreshTime() != null) this.setGpsLastRefreshTime(device.getGpsLastRefreshTime());
		if (device.getGpsLat() != null) this.setGpsLat(device.getGpsLat());
		if (device.getGpsLong() != null) this.setGpsLong(device.getGpsLong());
		if (device.getLoginTime() != null) this.setLoginTime(device.getLoginTime());
	}
}
