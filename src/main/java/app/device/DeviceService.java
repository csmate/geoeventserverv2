package app.device;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by lacka on 06/05/15.
 */
@Stateless
public class DeviceService {
    @PersistenceContext
    private EntityManager em;

    public Device getByUserId(Long id){
        Device ret = null;
        try {
            ret = em.createQuery("select d from Device d where d.profileID = :id", Device.class)
                .setParameter("id", id)
                .getSingleResult();
        } catch (NoResultException e){}
        return ret;
    }

    public Device getByDeviceId(String key) {
        Device ret = null;
        try {
            ret = em.createQuery("select d from Device d where d.deviceID = :key", Device.class)
                .setParameter("key", key)
                .getSingleResult();
        } catch (NoResultException e){
            e.printStackTrace();
        }
        return ret;
    }


    public Device getAttachedDevice(Long id) {
        return em.find(Device.class, id);
    }

    public boolean save(Device device) {
        boolean ret = true;
        try {
            em.persist(device);
        } catch (Exception e){
            ret = false;
        }
        return ret;
    }

    public boolean merge(Device device) {
        boolean ret = true;
        try {
            em.merge(device);
        } catch (Exception e){
            ret = false;
        }
        return ret;
    }

    private String getEventNearDevicesQuery =
            "SELECT filtered.deviceid FROM (" +
            "    SELECT *, 3956 * 2 * ASIN(SQRT(POWER(SIN((:latitude - devices.gpslat) * pi() / 180 / 2), 2) + COS(:latitude * pi()/180) * COS(devices.gpslat * pi() / 180) * POWER(SIN((:longitude - devices.gpslong) * pi() / 180 / 2), 2))" +
            ") AS distance FROM devices WHERE devices.recvpush = true ORDER BY distance ASC LIMIT :limitrow) AS filtered " +
            "WHERE filtered.distance < :dist";


    public List getDevicesProcedure(Double longitude, Double latitude, Integer distance, Integer limitRow) {
        return em.createNativeQuery(getEventNearDevicesQuery)
                .setParameter("longitude", longitude)
                .setParameter("latitude", latitude)
                .setParameter("limitrow", limitRow)
                .setParameter("dist", distance)
                .getResultList();
    }

    public List getDevices2Procedure(String event) {
        String sqlQuery = "SELECT r.deviceid FROM responds r WHERE r.eventid = '" + event + "'";
        return em.createNativeQuery(sqlQuery).getResultList();
    }

}
