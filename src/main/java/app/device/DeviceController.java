package app.device;

import com.google.gson.Gson;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/device")
public class DeviceController {
	@Inject
	private DeviceService deviceService;

	@GET
	@Path("/id/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("id") String id) {
		Device device = deviceService.getByUserId(Long.parseLong(id));
		if(device == null) {
			return Response.status(500).entity("Device not found").build();
		} else {
			return Response.status(200).entity(device).build();
		}
	}

	@GET
	@Path("/key/{key}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByDeviceId(@PathParam("key") String key) {
		Device device = deviceService.getByDeviceId(key);
		if(device == null) {
			return Response.status(500).entity("Device not found").build();
		} else {
			return Response.status(200).entity(device).build();
		}
	}
	
	
	@POST
	@Path("/registerorupdate")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerOrUpdate(Device device) {
		Device storedDevice = deviceService.getByUserId(device.getProfileID());
		if(storedDevice != null) {
			storedDevice = deviceService.getAttachedDevice(storedDevice.getId());
			storedDevice.update(device);
			storedDevice.setActive(true);
			boolean success = deviceService.merge(storedDevice);
			if(success) {
				return Response.status(200).entity("Device update success").build();
			} else {
				return Response.status(500).entity("Device update failed").build();
			}
		} else {
			boolean success = deviceService.save(device);
			if(success) {
				return Response.status(200).entity("Device save success").build();
			} else {
				return Response.status(500).entity("Device save failed").build();
			}
		}
	}

	@GET
	@Path("/disable/{id}")
	public Response disable(@PathParam("id") String id) {
		Device device = deviceService.getByUserId(Long.parseLong(id));
		if (device != null) {
			device = deviceService.getAttachedDevice(device.getId());
			device.setActive(false);
			boolean success = deviceService.merge(device);
			if (success) {
				return Response.status(200).entity("Device disable success").build();
			} else {
				return Response.status(500).entity("Device disable failed").build();
			}
		} else {
			return Response.status(500).entity("Device not found").build();
		}
	}

	@POST
	@Path("/set/gps")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response setDevice(DeviceGpsSetupRQ req) {
		System.out.println(new Gson().toJson(req));
		Device storedDevice = deviceService.getByUserId(req.getUserId());
		if(storedDevice != null) {
			storedDevice = deviceService.getAttachedDevice(storedDevice.getId());
			storedDevice.setGpsEnabled(req.isGps());
			boolean success = deviceService.merge(storedDevice);
			if (success) {
				return Response.status(200).entity("Device setup success").build();
			} else {
				return Response.status(500).entity("Device setup failed").build();
			}
		} else {
			return Response.status(500).entity("Device not found " + req.getUserId()).build();
		}
	}

	@POST
	@Path("/set/push")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response setDevice(DevicePushSetupRQ req) {
		System.out.println(new Gson().toJson(req));
		Device storedDevice = deviceService.getByUserId(req.getUserId());
		if(storedDevice != null) {
			storedDevice = deviceService.getAttachedDevice(storedDevice.getId());
			storedDevice.setRecvPush(req.isPush());
			boolean success = deviceService.merge(storedDevice);
			if (success) {
				return Response.status(200).entity("Device setup success").build();
			} else {
				return Response.status(500).entity("Device setup failed").build();
			}
		} else {
			return Response.status(500).entity("Device not found " + req.getUserId()).build();
		}
	}
}