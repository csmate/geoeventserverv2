package app.device;

/**
 * Created by lacka on 24/01/16.
 */
public class DeviceGpsSetupRQ {
    private Long userId;
    private boolean gps;

    public DeviceGpsSetupRQ() {}

    public DeviceGpsSetupRQ(Device device) {
        this.userId = device.getProfileID();
        this.gps = device.getGpsEnabled();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public boolean isGps() {
        return gps;
    }

    public void setGps(boolean gps) {
        this.gps = gps;
    }
}
