package app.event.respond;

import app.device.Device;
import app.device.DeviceService;
import app.event.create.EventController;
import app.event.create.EventService;
import app.event.create.Event;
import app.user.User;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.gson.Gson;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by lacka on 20/01/16.
 */
@Path("/response")
public class ResponseController {
    @Inject
    private ResponseService responseService;
    @Inject
    private DeviceService deviceService;
    @Inject
    private EventService eventService;

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") String id) {
        List<Respond> list = responseService.getByEventId(id);
        return Response.status(200).entity(new ResponseCount(list.size())).build();
    }

    @POST
    @Path("/send")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response respond(RespondEventRQ req) {

        String eventID = req.getEvent().getAppEventId();
        Event event = eventService.get(eventID);
        User user = req.getUser();
        String longitude = String.valueOf(req.getLongitude());
        String latitude = String.valueOf(req.getLatitude());
        Long time = req.getTime();

        double lon = -1, lat = -1;
        if(longitude != null && latitude != null) {
            lon = Double.valueOf(longitude);
            lat = Double.valueOf(latitude);
        }
        Device device = deviceService.getByUserId(user.getId());
        List<Respond> responses = responseService.getByEventAndDeviceId(event.getAppEventId(), device.getDeviceID());
        boolean notResponded = responses.size() < 1;

        if (device.getDeviceID() != null && notResponded) {
            Respond respond = new Respond(time, device.getDeviceID(), event.getAppEventId(), lon, lat);
            boolean success = responseService.save(respond);

            if(success)	{
                gcm2(event.getDeviceId(), "3", "GeoEvent", "Event responded", new Gson().toJson(event), 1);
                return Response.status(200).entity("Event response sent").build();
            } else {
                return Response.status(500).entity("Event response save failed").build();
            }
        } else {
            return Response.status(200).entity("Event response failed").build();
        }
    }

    private String getParam(Map<String, List<InputPart>> params, String key) throws IOException {
        return params.get(key).get(0).getBodyAsString();
    }

    private void gcm2(String recipient, String action, String title, String text, String event, int retry){
        Sender sender = new Sender(EventController.API_KEY);
        Message message = new Message.Builder()
                .addData("action", action)
                .addData("title", title)
                .addData("text", text)
                .addData("event", event)
                .build();
        try {
            System.out.println(message + " | " + recipient);
            Result result = sender.send(message, recipient, 1);
            if (result.getMessageId() != null) {
                // Success
                String canonicalRegId = result.getCanonicalRegistrationId();
                if (canonicalRegId != null) {
                    // same device has more than one registration id.Update it
                }
            } else {
                // Error occurred
                String error = result.getErrorCodeName();
                System.out.println(error);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
