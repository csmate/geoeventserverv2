package app.event.respond;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lacka on 07/05/15.
 */
@Stateless
public class ResponseService {
    @PersistenceContext
    private EntityManager em;

    public List<Respond> getByEventId(String id) {
        List<Respond> list = new ArrayList<Respond>();
        try {
            list = em.createQuery("select r from Respond r where r.eventID = :eventID", Respond.class)
                .setParameter("eventID", id)
                .getResultList();
        } catch (Exception e) {}
        return list;
    }

    public List<Respond> getByEventAndDeviceId(String eventid, String deviceid) {
        List<Respond> list = new ArrayList<Respond>();
        try {
            list = em.createQuery("Select r from Respond r where r.deviceID = :dID AND r.eventID = :eID", Respond.class)
                .setParameter("dID", deviceid)
                .setParameter("eID", eventid)
                .getResultList();
        } catch (Exception e) {}
        return list;
    }

    public boolean save(Respond resp) {
        boolean ret = true;
        try {
            em.persist(resp);
        } catch (Exception e){
            ret = false;
        }
        return ret;
    }
}
