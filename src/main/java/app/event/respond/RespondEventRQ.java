package app.event.respond;

import app.event.create.Event;
import app.user.User;

import java.io.Serializable;

/**
 * Created by lacka on 29/01/16.
 */
public class RespondEventRQ implements Serializable {
    private String method;
    private Event event;
    private User user;
    private double longitude;
    private double latitude;
    private long time;

    public RespondEventRQ() {}

    public RespondEventRQ(String respondEvent, Event event, User user, double longitude, double latitude, long timeMillis) {
        this.method = respondEvent;
        this.event = event;
        this.user = user;
        this.longitude = longitude;
        this.latitude = latitude;
        this.time = timeMillis;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
