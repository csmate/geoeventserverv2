package app.event.respond;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "responds")
public class Respond implements Serializable {

	private static final long serialVersionUID = 7125195075935464857L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private Long respondTime;

	private String deviceID;

	private String eventID;

	private Double GPSLong;

	private Double GPSLat;

	public Respond() {
	}

	public Respond(Long respondTime, String deviceID, String eventID,
			Double gPSLong, Double gPSLat) {
		super();
		this.respondTime = respondTime;
		this.deviceID = deviceID;
		this.eventID = eventID;
		GPSLong = gPSLong;
		GPSLat = gPSLat;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRespondTime() {
		return respondTime;
	}

	public void setRespondTime(Long respondTime) {
		this.respondTime = respondTime;
	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public String getEventID() {
		return eventID;
	}

	public void setEventID(String eventID) {
		this.eventID = eventID;
	}

	public Double getGPSLong() {
		return GPSLong;
	}

	public void setGPSLong(Double gPSLong) {
		GPSLong = gPSLong;
	}

	public Double getGPSLat() {
		return GPSLat;
	}

	public void setGPSLat(Double gPSLat) {
		GPSLat = gPSLat;
	}

	
}
