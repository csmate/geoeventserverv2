package app.event.list;

import app.event.create.Event;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lacka on 25/01/16.
 */
public class Events {
    private List<Event> events = new ArrayList<>();

    public Events() {}

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }
}
