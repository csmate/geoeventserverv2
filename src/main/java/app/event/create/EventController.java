package app.event.create;

import app.controllers.PictureController;
import app.device.Device;
import app.device.DeviceService;
import app.models.NotifiedFromCreateEvent;
import app.models.Picture;
import app.services.NotificationService;
import app.services.PictureService;
import app.utils.PushAction;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by lacka on 06/05/15.
 */
@Path("/event")
public class EventController {
    public static final String API_KEY = "AIzaSyDPFmGKNmJcclFINWEF5l_NV1GnE9Ojofs";
    public static final int RETRY = 1;

    @Inject
    private DeviceService deviceService;

    @Inject
    private NotificationService notifService;

    @Inject
    private EventService eventService;

    @Inject
    private PictureService pictureService;

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(CreateEventRQ req) {
        int deviceNumber = createEventNotif(req);
        if (deviceNumber > 0) {
            return Response.status(200).entity("Notification sent from event creation to " + deviceNumber + " devices").build();
        } else {
            return Response.status(200).entity("Devices not found").build();
        }
    }

    @POST
    @Path("/upload")
    @Consumes("multipart/form-data")
    public Response upload(MultipartFormDataInput parts) {
        int notificationNumber = uploadPictureNotif(parts);
        if (notificationNumber > 0) {
            return Response.status(200).entity("Notification sent from event file upload to " + notificationNumber + " devices").build();
        } else {
            return Response.status(200).entity("Devices not found").build();
        }

    }

    private int createEventNotif(CreateEventRQ req) {
        int deviceNumber = 0;
        try {
            String title = req.getTitle();
            String text = req.getText();
            Event event = req.getEvent();
            System.out.println("Event: " + event);
            Long userId = event.getUser().getId();

            Double longitude = event.getGpsLong();
            Double latitude = event.getGpsLat();
            Device device = deviceService.getByUserId(userId);
            if (longitude != null && latitude != null && device != null) {
                List<String> devices = getDevices(longitude, latitude, device.getDeviceID());
                if (devices.size() > 0) {
                    deviceNumber = devices.size();
                    try {
                        for(String d : devices){
                            NotifiedFromCreateEvent notif = new NotifiedFromCreateEvent();
                            notif.setEventID(event.getAppEventId());
                            notif.setOrigin(String.valueOf(userId));
                            notif.setDestination(d);
                            notifService.saveCreateNotif(notif);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    sendPushNotification(devices, PushAction.EVENT_CREATE, title, text, new Gson().toJson(event));
                }
                System.out.println("Event create save: " + new Gson().toJson(event));
                eventService.save(event);
            } else {
                throw new Exception("Unknown location or device");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deviceNumber;
    }

    private String getParam(Map<String, List<InputPart>> params, String key) throws IOException {
        return params.get(key).get(0).getBodyAsString();
    }

    private List<String> getDevices(Double longitude, Double latitude, String deviceID) {
        List<String> list = new ArrayList<String>();
        try {
            list = deviceService.getDevicesProcedure(longitude, latitude, 100, 100);
            if (list.contains(deviceID)) {
                list.remove(deviceID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("getDevicesProcedure query size:" + list.size());
        return list;
    }

    private void sendPushNotification(List<String> recipients, PushAction action, String title, String text, String event){
        Sender sender = new Sender(API_KEY);
        Message message = new Message.Builder()
                .addData("action", action.toString())
                .addData("title", title)
                .addData("text", text)
                .addData("event", event)
                .build();
        try {
            MulticastResult multiResult = sender.send(message, recipients, RETRY);
            List<Result> results = multiResult.getResults();

            for (int i = 0; i < results.size(); i++) {
                Result result = results.get(i);

                if (result.getMessageId() != null) {
                    // Success
                    String canonicalRegId = result.getCanonicalRegistrationId();
                    if (canonicalRegId != null) {
                        // same device has more than one registration id.Update it
                    }
                } else {
                    // Error occurred
                    String error = result.getErrorCodeName();
                    System.out.println(error);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int uploadPictureNotif(MultipartFormDataInput parts) {
        int notificationNumber = 0;
        Map<String, List<InputPart>> params = parts.getFormDataMap();

        try {
            String title = getParam(params, "title");
            String text = getParam(params, "text");
            String pictureJson = getParam(params, "pictureJson");
            InputPart file = null;
            String filePath = null;
            if (params.containsKey("file")) {
                file = params.get("file").get(0);

                filePath = processFile(file);
            }

            Picture pic = new Gson().fromJson(pictureJson, Picture.class);

            if (filePath != null) {
                pic.setFileData(filePath);
            }

            Event ev = eventService.get(pic.getEventID());

            if(ev != null) {
                pictureService.save(pic);
                List<String> devices = getDevices2(pic.getEventID(), pic.getDeviceID());
                if (devices.size() > 0) {
                    notificationNumber = devices.size();
                    sendPushNotification(devices, PushAction.EVENT_UPDATE, title, text, new Gson().toJson(ev));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return notificationNumber;
    }

    private String processFile(InputPart file) throws IOException {
        MultivaluedMap<String, String> header = file.getHeaders();
        String fileName = getFileName(header);

        InputStream inputStream = file.getBody(InputStream.class, null);

        byte [] bytes = IOUtils.toByteArray(inputStream);

        String fileDir = PictureController.UPLOAD_BASE + PictureController.PICTURE_FOLDER;
        String httpPath = PictureController.HTTP_BASE + PictureController.PICTURE_FOLDER + fileName;
        String filePath = fileDir + fileName;

        generateDirStructure(fileDir);
        writeFile(bytes, filePath);

        return httpPath;
    }

    private void generateDirStructure(String uploadDir) {
        new File(uploadDir).mkdirs();
    }

    private List<String> getDevices2(String event, String myDevice) {
        List<String> list = new ArrayList<String>();
        try {
            list = deviceService.getDevices2Procedure(event);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Devices size:" + list.size());
        return list;
    }

    private String getFileName(MultivaluedMap<String, String> header) {
        String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

        for (String filename : contentDisposition) {
            if ((filename.trim().startsWith("filename"))) {
                String[] name = filename.split("=");
                String finalFileName = name[1].trim().replaceAll("\"", "");
                return finalFileName;
            }
        }
        return "unknown";
    }

    //save to somewhere
    private void writeFile(byte[] content, String filename) throws IOException {

        File file = new File(filename);

        if (!file.exists()) {
            file.createNewFile();
        }

        FileOutputStream fop = new FileOutputStream(file);

        fop.write(content);
        fop.flush();
        fop.close();

    }
}
