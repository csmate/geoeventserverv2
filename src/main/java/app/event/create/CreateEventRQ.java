package app.event.create;

import java.io.Serializable;

/**
 * Created by lacka on 02/02/16.
 */
public class CreateEventRQ implements Serializable {
    private String title;
    private String text;
    private Event event;

    public CreateEventRQ() {
    }

    public CreateEventRQ(String title, String text, Event event) {
        this.title = title;
        this.text = text;
        this.event = event;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
