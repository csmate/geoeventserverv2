package app.event.create;

import app.event.list.Events;
import app.user.User;

import javax.annotation.ManagedBean;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import java.util.ArrayList;

/**
 * Created by lacka on 06/05/15.
 */

@ManagedBean
public class EventService {
    @PersistenceContext
    private EntityManager em;

    @Resource
    private UserTransaction utx;

    public Event get(String event) {
        Event ret = null;
        try{
            ArrayList<Event> list = (ArrayList<Event>)em.createQuery("select e from Event e where e.appEventId = :event", Event.class)
                    .setParameter("event", event).setMaxResults(1).getResultList();
            if (list.size() > 0) {
                ret = list.get(0);
            }
        } catch (NoResultException e) {}
        return ret;
    }

    public boolean save(Event event) {
        boolean ret = false;
        User user = em.find(User.class, event.getUser().getId());
        try {
            utx.begin();
            event.setUser(user);
            em.persist(event);
            utx.commit();
            ret = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public Events getEvents(String sort) {
        String getEventsQuery = "SELECT e FROM Event e";
        if (sort != null && !sort.isEmpty() && !"default".equals(sort)) {
            getEventsQuery += " ORDER BY e." + sort;
        }
        Events ret = new Events();
        try{
            ArrayList<Event> list = (ArrayList<Event>)em.createQuery(getEventsQuery, Event.class)
                    .getResultList();
            ret.setEvents(list);
        } catch (NoResultException e) {}
        return ret;
    }
}
