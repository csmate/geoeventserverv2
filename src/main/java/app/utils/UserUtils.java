package app.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class UserUtils {
	private static final String SHA_MODE = "SHA-256";

	public static SecurePair generateHash(String pwd) {
		SecurePair pair = null;
		try {
			String salt = getSalt();
			pair = new SecurePair(get_SHA_SecurePassword(SHA_MODE, pwd, salt), salt);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return pair;
    }
	
	public static String calculateHash(SecurePair pair) {
		return get_SHA_SecurePassword(SHA_MODE, pair.getPassword(), pair.getSalt());
    }
	
	public static String calculateHash(String password, String salt) {
		return get_SHA_SecurePassword(SHA_MODE, password, salt);
    }
 
    private static String get_SHA_SecurePassword(String mode, String passwordToHash, String salt) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance(mode);
            md.update(salt.getBytes());
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } 
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }
    
    private static String getSalt() throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt.toString();
    }
	
}
