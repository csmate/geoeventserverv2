package app.utils;

/**
 * Created by lacka on 27/08/15.
 */
public enum PushAction {
    EVENT_CREATE(1),
    EVENT_UPDATE(2);

    private final int value;

    PushAction(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}