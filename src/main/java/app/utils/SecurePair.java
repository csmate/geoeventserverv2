package app.utils;

public class SecurePair {
	
	private String pwd;
	private String salt;
	
	public SecurePair(String pwd, String salt) {
		this.pwd = pwd;
		this.salt = salt;
	}
	
	public String getPassword() {
		return pwd;
	}
	
	public String getSalt(){
		return salt;
	}
}